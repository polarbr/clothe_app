Rails.application.routes.draw do
  devise_for :users, :controllers => {
    :registrations => 'registrations',
    :sessions => 'sessions'
}
  
  root 'home#index'
  resources :clothes, except: :index
  resources :users, only: [:show, :index, :destroy]
end
