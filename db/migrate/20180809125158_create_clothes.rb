class CreateClothes < ActiveRecord::Migration[5.2]
  def change
    create_table :clothes do |t|
      t.string :item
      t.string :color
      t.date :date
      t.text :memo
      t.string :img
      t.integer :user_id

      t.timestamps
    end
    add_index :clothes, [:user_id, :item, :color]
  end
end
