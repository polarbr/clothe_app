class Clothe < ApplicationRecord
  mount_uploader :img, ImgUploader
  
  belongs_to :user
  validates :user_id, presence: true

  validates :memo, length: { maximum: 140 }

  def self.search(search)
    if search
      where(['item LIKE ? or color LIKE ?', "%#{search}%", "%#{search}%"])
    else
      all
    end
  end
end
