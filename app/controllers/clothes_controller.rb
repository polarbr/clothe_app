class ClothesController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: [:show, :edit, :update, :destroy]

  def show
    @clothe = Clothe.find(params[:id])
  end

  def new
    @clothe = Clothe.new
  end

  def create
    @clothe = current_user.clothes.create(clothe_params)
    if @clothe.save
      flash[:success] = "洋服を追加したよ！"
      redirect_to current_user
    else
      @clothe = []
      redirect_to current_user
    end
  end

  def edit
    @clothe = Clothe.find(params[:id])
  end

  def update
    @clothe = Clothe.find(params[:id])
    if @clothe.update(clothe_params)
      redirect_to current_user
    else
      redirect_to edit_clothe_path
    end
  end

  def destroy
    @clothe = Clothe.find(params[:id])
    @clothe.destroy
    flash[:success] = "洋服を削除したよ"
    redirect_to current_user
  end

  private
    def clothe_params
      params.require(:clothe).permit(:item, :color, :date, :memo, :img, :use_id)
    end

    def correct_user
      @clothe = current_user.clothes.find_by(id: params[:id])
      redirect_to root_url if @clothe.nil?
    end
end