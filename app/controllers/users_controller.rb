class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:index, :show, :destroy]
  before_action :admin_user, only: [:index, :destroy]

  def index
      @users= User.page(params[:page])
  end

  def show
    @user = User.find(params[:id])
    @clothes = @user.clothes.page(params[:page]).search(params[:search])
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User destroyed."
    redirect_to users_url
  end

  private
  def admin_user
    redirect_to(root_path) unless current_user.admin?
  end

end
