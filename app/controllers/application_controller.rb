class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_current_user

  def set_current_user
    @current_user = User.find_by(id: session[:user_id])
  end

  private
    def clothes_search_params
      params.require(:q).permit(:item_or_color_cont)
    end
end
