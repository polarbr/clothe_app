json.extract! clothe, :id, :item, :color, :date, :memo, :img, :user_id, :created_at, :updated_at
json.url clothe_url(clothe, format: :json)
